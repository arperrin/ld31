///scr_post_error(message)
message = string(argument0);
line = 12;
char_width = 12;
var xx =  528 - (string_length(message) * char_width) / 2;
var yy =  320 - line;

new_line = instance_create(xx, yy, obj_error_message);
with (new_line)
{
  message = other.message; 
}

