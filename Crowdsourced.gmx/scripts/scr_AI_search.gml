/// scr_AI_search(fighter)
/*
  Find enemy.
*/
me = argument0;
me.doing = "searching";


with (obj_fighter)
{
  // This is following the first enemy it finds. Fix this later?
  if (distance_to_object(other.me) < room_width/2 && team != other.me.team)
  {
//    show_debug_message(string(other.me.name) + ": Found " + string(name));
    return id;
  }
}

return noone;
