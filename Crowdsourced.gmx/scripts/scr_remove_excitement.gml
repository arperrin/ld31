///scr_remove_excitement(amount, action)
with (obj_audience)
{ 
  excitement -= other.argument0;
  show_debug_message("Remove " + string(argument0) +" excitement: " + string(argument1));
  if (last_action == other.argument1)
  {
    excitement -= other.argument0;
    show_debug_message("Remove " + string(argument0) +" excitement (repeat action): " + string(argument1));
  }
  last_action = other.argument1;  
}
