///scr_AI_patrol(fighter)
/* 
Patrolling is done when a fighter has no target. 
They will wander until they find a target.  

Patrolling:
Pick a random direction and start moving toward a target spot.
*/

with(argument0)
{
  doing = "patrolling";
  
  // Adding an angular offset to the random angle based on quadrant to try and force
  // patrolling away from corners.
  offset = 0;
  if (x < room_width / 2 && y < room_height / 2) // upper left
  {
    offset = 90*3;
  }
  else if (x < room_width /2 && y > room_height /2) // lower left
  {
    offset = 90*0;
  }
  else if (x > room_width /2 && y < room_height / 2) // upper right
  {
    offset = 90*2;
  }
  else // lower right
  {
    offset = 90*1; 
  }  
  
  // Too close to a wall? Steer away from wall.
//  if (instance_nearest(x, y, obj_arena_border) < 30)
  direction = random(90) + offset;
//  else
  //  direction = random(365);
  
  image_angle = direction;
  motion_set(direction, walk_speed);
}