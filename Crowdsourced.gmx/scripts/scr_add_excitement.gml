///scr_add_excitement(amount, action)

with(obj_audience)
{
  excitement += other.argument0 / 2;
  show_debug_message("Add " + string(argument0/2) +" excitement: " + string(argument1));
  if (last_action != other.argument1)
  {
  show_debug_message("Add " + string(argument0/2) +" excitement (new action!): " + string(argument1));
    excitement += other.argument0 / 2;
  }
    
  last_action = other.argument1;
}


