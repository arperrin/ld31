///scr_AI_pkg_melee(fighter)
/*
AI priority package for melee fighters

1. Patrol (if no target)
2. Search (for target)
3. Attack (if target is close)
4. Chase (if target is not close)

*/

if (target == noone && tx == 0 && ty == 0)
{
  scr_AI_patrol(argument0);
}
else if (target == noone && (abs(tx - x) < 2 || abs(ty - y) < 2))
{
  scr_AI_patrol(argument0);
}
