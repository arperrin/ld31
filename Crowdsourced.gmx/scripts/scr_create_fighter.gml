///scr_create_fighter(x, y, team)

// Generate a name
randomize();
new_name = "nameless";
new_name = choose("Reggie", "Stew", "Jack", "Jim", "Jill", "Jonny", "Anthony", "Peter");

nf = instance_create(argument0, argument1, obj_fighter);
with (nf)
{
  team = argument2;
  name = other.new_name;
}


