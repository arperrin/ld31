///scr_init_fighters
/* 
Initializes fighter values which should be unique
throughout all fighters or at least varied enough to be interesting.
(e.g. Name)
*/

obj_stats.fight_start = current_time;

randomize();
names = ds_list_create();
ds_list_add(names, "Bob");
ds_list_add(names, "Steve");
ds_list_add(names, "Stan");
ds_list_add(names, "Andrew");
ds_list_add(names, "Josh");
ds_list_add(names, "Bill");
ds_list_shuffle(names);

i = 0;
with(obj_fighter)
{
  name = other.names[| other.i];
  other.i++;
  if (team == "green") obj_stats.green_fighters++;
  else obj_stats.red_fighters++;
}

ds_list_destroy(names);
